Bilirubin Risk Chart Role
=========================

This is a role for ansible-galaxy for URL based roles

Requirements
------------


Role Variables
--------------

These variables are external to the role, but used by the role:

Variable                                   | Desc
------------------------------------------ | --------------
hosting_username                           | Name of the user hosting the application
hosting_user_home                          | Home directory of the user hosting the application
bilirubin_risk_chart_server_external_host  | Host of the application
bilirubin_risk_chart_server_external_port  | Port of the application
bilirubin_risk_chart_project_version       | Version of the app
use_secure_http                            | Should the inbound requests use HTTPS when calling the services?


Dependencies
------------


Example Playbook
----------------

- hosts: all
  become: yes
  vars_files:
    - roles/common/defaults/main.yml
    - roles/linux/defaults/main.yml
    - environments/{{env}}.yml
  roles:
    - common
    - { role: bilirubin-risk-chart-role }

License
-------

Apache2

Author Information
------------------

